export type User = {
    name: string,
    email: string,
    phone: string,
    location: string,
    skills:any,
    education: string,
    languages: any,
    photo:string
}