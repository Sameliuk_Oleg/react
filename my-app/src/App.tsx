import React from 'react';
import {Card} from "./components/card/card";
import './App.css';
import {profile} from "./profile";

function App() {
  return (
    <div className="App">
      <Card user = {profile}/>

    </div>
  );
}

export default App;
