import React  from 'react';
import { User } from '../../typedef';

interface Props{
    user:User
}


export  const Card= ({user}: Props) => {
    return(
        <div className="app">
            <div className="app__header">
                <img src={user.photo} alt="image"/>
            </div>
            <div className="app__body">
                <h1 className="app__full-name">
                    {user.name}
                </h1>
                <a href={user.email} className="app__email">
                    Email: {user.email}
                </a>
                <p className="app__phone">
                    Phone: {user.phone}
                </p>
                <p className="app__location">
                    Location: {user.location}
                </p>
                <ul className="app__skills">
                    <li className="app__skills-list">
                        Skill: {user.skills.first}
                    </li>
                    <li className="app__skills-list">
                        Skill: {user.skills.second}
                    </li>
                    <li className="app__skills-list">
                        Skill: {user.skills.third}
                    </li>
                    <li className="app__skills-list">
                        Skill: {user.skills.fourth}
                    </li>
                    <li className="app__skills-list">
                        Skill: {user.skills.fifth}
                    </li>
                    <li className="app__skills-list">
                        Skill: {user.skills.sixth}
                    </li>
                    <li className="app__skills-list">
                        Skill: {user.skills.seventh}
                    </li>
                </ul>
                <p className="app__education">
                    Education: {user.education}
                </p>
                <ul className="app__language">
                    <li className="app__language-list">
                        Language: {user.languages.native}
                    </li>
                    <li className="app__language-list">
                        Language: {user.languages.intermediate}
                    </li>
                </ul>
            </div>
        </div>
    )
}