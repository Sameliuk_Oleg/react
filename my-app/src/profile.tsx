import {url} from "inspector";

export const profile = {
    name:"Sameliuk Oleg",
    email:"olegsameluk82@gmail.com",
    phone:"+380971863379",
    location:"Cherkassy",
    skills:{
        first: "HTML",
        second: "CSS (SCSS)",
        third: "JavaScript",
        fourth: "Responsive web design",
        fifth: "WordPress",
        sixth: "Drupal",
        seventh: "Git"
    },
    education:"B. Khmelnytsky National University Research Institute of Information and Educational Technologies, ACIT, Cherkassy Higher, from 09.2017 to 06.2021",
    languages:{
        native: "Ukrainian and Russian – Native",
        intermediate: "English –Intermediate"
    },
    photo:"https://lh3.googleusercontent.com/OL559ES5kcTN6RFX5jR9NoWTQj_ysLVLQ_5DLKi9N4mrTx_vSRqb62xy55hR-smug280U-Tj4SsazrNY-YbgKhax1lAb52_N1tpDOy_dlOBpJNX7-jFMeLEbdkwBi8ZC5PFertFg37DYBTPVLNh6FF_tVq4SWbhXoFqLsg_XoVLNZhl-hRu1Ae0n53SvJVPnMfCeNkuIT6Lp6cE_jQCMqNDIvS2XgfOicYCAytYvC25TiAIxl3I9OitW4VezDQVwy7OcE13-fLq41VTiJX8dTaOd6VxjQGljqlXGVgSFpfraE6OYD88gZnUkfPOHP71G4flxEgWzK5hUUUqgut9w0wjnyHhNU1swYWiFM9dvISVWux5SgyzAjx9BbeQMIAB5yCxOPOFra2e3hdOwNB9SOSqgmgDfcjWueq7yvGf60DcDYne85FRnRXSbyMJOquxdawhbDPsDNPV5Up2LRJSZk305tOZrIUKF0FcuPCNhBhFZVWzas-M74MIFGzK8oJipOBKQJABd3rBpwmrM6BD6QyeyyHCMuVrAPgVj7TK2BHCSL65qwmujmrAx7Vlc1Ev-9XxUGVtTZypt3BOMQbbAhqJR0-maxgKjd5gC85-YY9E0i9xrdafXOneLhiXKDZ6hV_lEsL9XfyqT6-tuqUBHIOpzfcyRn538k7vfu1ZuTgYABrjmAFUG3RbS4P9E=w1605-h903-no?authuser=0"
}