import React from "react";


function  Pagination  ({perPage, refresh}: { perPage: number, refresh: any }): JSX.Element  {
    console.log('test Pagination', perPage)
    return (<>
        {new Array(10).fill(null).map((_, idx) => {
            return (
                <div className={'button_block'}>
                    <button
                        onClick={(event: any) => refresh(event.target.value, perPage)}
                        value={idx + 1}>{idx + 1}
                    </button>
                </div>
            )
        })}
    </>)
}

export default Pagination;