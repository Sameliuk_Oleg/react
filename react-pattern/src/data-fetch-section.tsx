import {ReactElement, useEffect, useState} from 'react';

type RefreshFn = (page: number, perPage: number) => void;

export type CallbackParams<T> = { data: T | null, refresh: RefreshFn, page: number, perPage: number,    }

export interface Props<T> {
    children: (params: CallbackParams<T>) => ReactElement,
    url: string,
}

export function DataFetchSection<T>(props: Props<T>) {
    const [data, setData] = useState<T | null>(null);
    const [page, setPage] = useState(1);
    const [perPage, setPerPage] = useState(10);

    useEffect(() => {
        fetchData(page, perPage);
    }, []);



    const fetchData = async (page:number, perPage:number) => {
        const response = await fetch(props.url +`?page=${page}&per_page=${perPage}`);
        const data = await response.json();
        setData(data);
        console.log(data)
    };

    return props.children({
        data,
        refresh: fetchData,
        page,
        perPage,

    });

}