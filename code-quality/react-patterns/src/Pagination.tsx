import React, {useState} from "react";


function Pagination({perPage, refresh}: { perPage: number, refresh: any }): JSX.Element {
    const [shown] = useState(true);
    return (<div key="{item}">
        {new Array(10).fill(null).map((_, idx) => {
            return (
                <div key={idx.toString()}>
                    {shown ?
                        <div className={'button_block'} key={idx.toString()}>
                            <button
                                onClick={(event: any) => refresh(event.target.value, perPage)}
                                value={idx + 1} key={(idx + 1).toString()} className={"button"}>
                                {idx + 1}
                            </button>
                        </div>
                        : null}
                </div>
            )
        })}
    </div>)
}

export default Pagination;
