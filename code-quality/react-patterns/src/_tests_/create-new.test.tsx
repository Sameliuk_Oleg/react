import React from "react";
import {shallow} from "enzyme";
import Pagination from "../Pagination";


enum TaskStatus {
    REVIEW = "Review"
}

// @ts-ignore
const createShallowed = () => shallow(<Pagination status={TaskStatus.REVIEW}/>)

describe("Create new component <Pagination />", () => {
    describe("when render", () => {
        it("should not show card", () => {
            const wrapper = createShallowed();
            expect(wrapper.find('.button_block').length).toBe(10)
        })
    });
});
