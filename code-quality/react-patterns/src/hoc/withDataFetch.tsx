import React, {JSXElementConstructor} from "react";
import {DataFetchSection, Props} from "../data-fetch-section";
import Pagination from "../Pagination";

function withDataFetch(ChildComp: JSXElementConstructor<any | string>) {

    return () => {
        return (
            <div>
                <DataFetchSection<Props<any>> url={`https://api.github.com/orgs/facebook/repos`}>
                    {
                        ({data, refresh, page, perPage}) => <>

                            <Pagination perPage={perPage} refresh={refresh}/>


                            <ChildComp props={data}/>

                        </>
                    }
                </DataFetchSection>
            </div>
        );
    };
}


const UserBlock: React.FC<{ props: Array<object>, avatar_url: string, name: string }> = ({props}) => {

    if (!props) {
        return null;
    }
    return (
        <div className={'users'}>
            {
                props.map((data: any) => {
                    return (
                        <div>
                            <img className={'avatar_image'} src={data.owner.avatar_url} alt={"description"}/>
                            <h2 className={'users_name'}>{data.name}</h2>
                        </div>
                    );
                })
            }
        </div>
    )
}

export const App = withDataFetch(UserBlock);


