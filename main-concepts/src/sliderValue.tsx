import {FC} from 'react';

interface Props {
    children: any
}

export const SliderValue: FC<Props> = ({children}) => {
    return (
        <div>
            {children}
        </div>
    );
};
