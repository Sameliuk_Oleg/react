import React from 'react';
import {Slider} from "./slider";
import {SliderValue} from "./sliderValue";
import './App.css';

export const Sliders = () => {
    return (
        <div className={"position_slider"}>
            <div>
                <h1>Slider first</h1>
                <Slider>
                    <SliderValue>
                        <img
                            src={'https://images.pexels.com/photos/417074/pexels-photo-417074.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500'}
                            alt="nature" className={"slider_item_size"}/>
                    </SliderValue>
                    <SliderValue>
                        <img src={'https://cdn.pixabay.com/photo/2015/12/01/20/28/road-1072823__340.jpg'} alt="nature"
                             className={"slider_item_size"}/>
                    </SliderValue>
                    <SliderValue>
                        <img src={'https://4kwallpapers.com/images/walls/thumbs_2t/582.jpg'} alt="nature"
                             className={"slider_item_size"}/>
                    </SliderValue>
                </Slider>
            </div>
            <div>
                <h2>Slider second</h2>
                <Slider>
                    <SliderValue>
                        <img
                            src={'http://edibleprint.com/pics/42_edible_3745.jpg'}
                            alt="nature" className={"slider_item_size"}/>
                    </SliderValue>
                    <SliderValue>
                        <img
                            src={'http://edibleprint.com/pics/10_edible_5126.jpg'}
                            alt="nature" className={"slider_item_size"}/>
                    </SliderValue>
                    <SliderValue>
                        <img src={'http://edibleprint.com/pics/5_edible_7425.jpeg'} alt="nature"
                             className={"slider_item_size"}/>
                    </SliderValue>
                </Slider>
            </div>
        </div>

    );
};
