import {FC, useState} from 'react';

interface Props {
    children: any,
}

export const Slider: FC<Props> = ({children}) => {
    const [valueSlider, valueButtonSlider] = useState(0);

    const next = () => {
        valueButtonSlider(valueSlider => valueSlider + 1);
    }

    const prev = () => {
        valueButtonSlider(valueSlider => valueSlider - 1);
    }
    return (
        <div>
            <div>
                <div key={valueSlider}>{children[valueSlider]}</div>
            </div>
            <button onClick={prev}>
                Prev
            </button>
            <button onClick={next}>
                Next
            </button>
        </div>
    );
};
