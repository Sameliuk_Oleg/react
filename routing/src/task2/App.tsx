import React from 'react';
import logo from './logo.svg';
import './App.css';
import {PhotoSlider} from "./slider";


function App() {
  return (
    <PhotoSlider/>
  );
}

export default App;
