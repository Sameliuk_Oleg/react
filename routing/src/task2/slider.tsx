import React, {useState} from "react";
import clsx from "clsx";
import {SliderComponent} from "./sliderComponent";
import {info} from "./slideinfo";


enum Slides {
    FirstSlide = 0,
    SecondSlide = 1,
    ThirdSlide = 2


}

const MAX_STEPS = 3;


export const PhotoSlider = () => {

    const [activeSlide, setActiveSlide] = useState(Slides.FirstSlide);

    const onNext = () => {
        setActiveSlide(activeSlide + 1)
    }
    const onPrevious = () => {
        setActiveSlide(activeSlide - 1)
    }

    const clickDot = (index: number) => {
        setActiveSlide(index);

    }


    return (
        <form id={'slider'} action={''} style={{marginTop: '20px'}}>

            {activeSlide === Slides.FirstSlide && <SliderComponent data={info[activeSlide]} style={{zIndex: 0}}/>}
            {activeSlide === Slides.SecondSlide && <SliderComponent data={info[activeSlide]}/>}
            {activeSlide === Slides.ThirdSlide && <SliderComponent data={info[activeSlide]}/>}
            <div style={{overflow: 'auto', marginTop: '-45px'}}>
                <div style={{textAlign: 'center'}}>
                    <button type={"button"} disabled={activeSlide === 0} onClick={onPrevious}
                            style={{marginRight: '500px', zIndex: 111, borderRadius: '15px', border: 'none'}}><img
                        src="https://iconmonstr.com/wp-content/g/gd/makefg.php?i=../assets/preview/2017/png/iconmonstr-arrow-72.png&r=0&g=0&b=0"
                        alt="" style={{width: '20px', height: '20px'}}/></button>
                    <button type={"button"} disabled={activeSlide === MAX_STEPS - 1} onClick={onNext}
                            style={{ borderRadius: '15px', border: 'none'}}><img
                        src="https://iconmonstr.com/wp-content/g/gd/makefg.php?i=../assets/preview/2017/png/iconmonstr-arrow-71.png&r=0&g=0&b=0"
                        alt="" style={{width: '20px', height: '20px'}}/></button>
                </div>
            </div>

            <div className={`step_block`}>
                {
                    new Array(MAX_STEPS).fill(null).map((_, idx) => {
                        const classess = clsx('step', {
                            step_active: idx === activeSlide
                        })
                        return <span key={String(idx)} className={classess} onClick={() => clickDot(idx)}/>
                    })
                }


            </div>


        </form>
    )
}



