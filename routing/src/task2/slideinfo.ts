export const info = [
    {
        photo: 'https://images.pexels.com/photos/417074/pexels-photo-417074.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
        photo_description: 'nature',
        text: 'Beautiful Nature Photos'
    },
    {
        photo: 'https://cdn.pixabay.com/photo/2015/12/01/20/28/road-1072823__340.jpg',
        photo_description: 'nature',
        text: 'Beautiful Nature Photos'
    },
    {
        photo: 'https://4kwallpapers.com/images/walls/thumbs_2t/582.jpg',
        photo_description: 'nature',
        text: 'Beautiful Nature Photos'
    }
]