import {info} from "./slideinfo";
import React, {useState} from "react";


export const SliderComponent = (props: any) => {


    return (
        <div style={{margin: `0 auto`, backgroundRepeat: `no-repeat`, textAlign: `center`, }}>
            <img src={props.data.photo} alt={props.data.photo_description} style={{margin: '0 auto',  width: `500px`, height: `200px`, borderRadius:`10px`}}/>
            <p style={{fontSize: `20px`,  color: `white`, marginTop: `-120px`}}> {props.data.text}</p>
        </div>
    )
}
