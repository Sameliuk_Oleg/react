import React from 'react';
import logo from './logo.svg';
// import './App.css';
import { Context_Api } from './countries_api';

function App() {
  return (
    <div className="App">
      <Context_Api/>
    </div>
  );
}

export default App;
