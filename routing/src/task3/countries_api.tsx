import React, {useEffect, useState} from "react"
import {AppContext} from "./app.context";
import {Country, Holidays, Lang} from "./typedef";



export const Context_Api = () => {
    // const [data, setData] = useState([])

    const [country, setCountry] = useState<Country>('RU');

    const [lang, setLang] = useState<Lang>('RU')
    const [holidays, setHolidays] = useState<Holidays>()

    // const fetchCountries = "https://gcp-test-yq2tp6xfda-ew.a.run.app/Api/v2/AvailableCountries"
    // const fetchCountries = `/countries.json`
    // const getData = () =>
    //     fetch(`${fetchCountries}`)
    //         .then((res) => res.json())

    // useEffect(() => {
    //     getData().then((data) => {
    //         setData(data)
    //         console.log('data', data)
    //     })
    // }, [])


    // const fetchH = `https://gcp-test-yq2tp6xfda-ew.a.run.app/api/v2/publicholidays/2017/${country}`
    let fetchH = ''
    if(country === lang){
         fetchH = `https://gcp-test-yq2tp6xfda-ew.a.run.app/api/v2/publicholidays/2017/${country}`
    }    else {
         fetchH = `/../${country}-${lang}.json`
    }
    useEffect(() => {
        fetch(`${fetchH}`)
            .then(result => result.json())
            .then((holiday) => {
                setHolidays(holiday)
            })
    }, [lang, country])



    const changeCountry = (country: any) => {

        setCountry(country)
    }

    const changeLang = (lang: any) => {
        setLang(lang)
    }

    const contextValue = {

        changeCountry: (country: Country) => setCountry(country),
        country,
        holidays,
        changeLang: (lang: Lang) => setLang(lang),
        lang
    };



    if (!holidays ) {

        return null;
    }
    return (

        <AppContext.Provider value={contextValue}>
            <div>

                <select onChange={(e) => changeCountry(e.target.value)}>
                    <option value={'RU'}>Russia</option>
                    <option value={'UA'}>Ukraine</option>
                </select>

                <select onChange={(e) => changeLang(e.target.value)}>
                    <option value={'RU'}>RU</option>
                    <option value={'UA'}>UA</option>
                </select>

                {holidays.map((country) =>
                    <ul>
                        <li>
                            <p>
                                {country.localName} - {country.date}
                            </p>
                        </li>
                    </ul>
                )}
            </div>
        </AppContext.Provider>
    )
}