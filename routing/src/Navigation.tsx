import React, {FC} from "react";
import {Link} from "react-router-dom";
import './style.css'

type Props = {};

export const Nav: FC<Props> = () => {
    return (
        <nav>
            <ul className={'nav'}>
                <li>
                    <Link className={'list'} to="/">Task 1</Link>
                </li>
                <li>
                    <Link className={'list'} to="/task2/App">Task 2</Link>
                </li>
                <li>
                    <Link className={'list'} to="/task3/App">Task 3</Link>
                </li>
                <li>
                    <Link className={'list'} to="/task4/task4_1/App">Task 4</Link>
                        <ul className={'child_list'}>
                            <li>
                                <Link className={'list'} to="/task4/task4_1/App">Sorted</Link>
                            </li>
                            <li>
                                <Link className={'list'} to="/task4/task4_2/App">Logger</Link>
                            </li>
                        </ul>


                </li>
            </ul>
        </nav>
    )
};
