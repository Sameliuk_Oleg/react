import React, {useEffect, useState} from "react"


import {useHasChanged,  useFilters} from './customHooks/useLogger';



export const Logger = () => {




    const [counter, setCounter] = useState(0)
    const {prevVal, isChanged}= useHasChanged(counter)



    useEffect(() => {
        if (isChanged) {
            console.log("counter has changed, prevValue="+prevVal + ", currentValue=" + counter);
        }
    });

    useEffect(() => {
        const interval = setInterval(() => {
            setCounter(counter => counter + 1);
        }, 1000);

        return () => {
            clearInterval(interval);
        };
    }, [])



   return(
       <p>
           Open console please
       </p>
   )


}
