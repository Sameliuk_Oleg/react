import { createContext } from "react";
import { Lang,  Country, Holidays} from "./typedef";

export type Context = {

    changeCountry: (country: Country) => void,
    country: Country,
    changeLang: (lang: Lang) => void,
    lang: Lang
}

export const AppContext = createContext<Context | null>(null);

