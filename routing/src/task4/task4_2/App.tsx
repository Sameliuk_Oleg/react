import React from 'react';

import './App.css';
import { Logger} from './countries_api';

function App() {
  return (
    <div className="App">
      <Logger/>
    </div>
  );
}

export default App;
