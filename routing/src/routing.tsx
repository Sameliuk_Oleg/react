import React, {lazy, Suspense} from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import {Nav} from "./Navigation";



const  ReactOverview = lazy(() => import ('./task1/App'));
const Slider = lazy(() => import ('./task2/App'));
const Context = lazy(() => import ('./task3/App'));
const Hooks = lazy(() => import ('./task4/App'));
const Sort = lazy(() => import ('./task4/task4_1/App'));
const Logger = lazy(() => import ('./task4/task4_2/App'));
const Errors = lazy(()=>import('./Error'));



export const Rout = () => (

    <Router>
        <Nav/>

        <Suspense fallback={<div>Loading...</div>}>
            <Switch>
                <Route exact path='/' component={ReactOverview}/>
                <Route  path='/task2' component={Slider}/>
                <Route  path='/task3' component={Context}/>
                <Route path="/task4" component={Hooks}>
                    <Route path="/task4/task4_1" component={Sort} />
                    <Route path="/task4/task4_2" component={Logger} />
                </Route>
                <Route  path="*" component={Errors}/>
            </Switch>

        </Suspense>

    </Router>

);