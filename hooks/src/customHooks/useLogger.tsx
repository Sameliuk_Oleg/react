import {useEffect, useRef} from "react"

export const useHasChanged= (val: any) => {
    const prevVal = usePrevious(val)
    const isChanged = prevVal !== val
    return {prevVal, isChanged}}

export const usePrevious = (value:any) => {
    const ref = useRef();
    useEffect(() => {
        ref.current = value;
    });
    return ref.current;
}

export const useFilters = (arr:any, functions:any) => {
    if (!arr || !functions) {return []}

    return arr.reduce(
        (arrReduce:any, arrElem:any) => {
            const isNotSkip = functions.reduce(
                (accum:any, func:any) => {
                    return accum && func(arrElem)
                }, true
            )

            if (isNotSkip) {
                arrReduce.push(arrElem)
            }
            return arrReduce
        },
        []
    )
}
