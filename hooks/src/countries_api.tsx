import React, {useEffect, useState} from "react"
import {AppContext} from "./app.context";
import {Country, Holidays, Lang} from "./typedef";
import {useHasChanged, usePrevious, useFilters} from './customHooks/useLogger';

export const Context_Api = () => {
    const [country, setCountry] = useState<Country>('RU');
    const [lang, setLang] = useState<Lang>('RU')
    const [holidays, setHolidays] = useState<Holidays>()

    const [counter, setCounter] = useState(0)
    const {prevVal, isChanged}= useHasChanged(counter)

    const sortedHolidays = useFilters(holidays, [
        (holiday:any) => holiday.localName == 'Новогодние каникулы' ,
        (holiday:any) => holiday.date == '2017-01-04' || holiday.date == '2017-01-06' ,
    ])

    useEffect(() => {
        console.log('try', sortedHolidays)
    }, [holidays])

    useEffect(() => {
        if (isChanged) {
            console.log("counter has changed, prevValue="+prevVal + ", currentValue=" + counter);
        }
    });

    useEffect(() => {
        const interval = setInterval(() => {
            setCounter(counter => counter + 1);
        }, 1000);

        return () => {
            clearInterval(interval);
        };
    }, [])

    let fetchH = ''
    if (country === lang){
        fetchH = `https://gcp-test-yq2tp6xfda-ew.a.run.app/api/v2/publicholidays/2017/${country}`
    } else {
        fetchH = `/${country}-${lang}.json`
    }

    const changeCountry = (country: any) => {setCountry(country)}
    const changeLang = (lang: any) => {setLang(lang)}

    useEffect(() => {
        fetch(`${fetchH}`)
            .then(result => result.json())
            .then((holiday) => {
                setHolidays(holiday)
            })
    }, [lang, country])

    if (!holidays ) {return null}

    const contextValue = {
        changeCountry: (country: Country) => setCountry(country),
        country,
        holidays,
        changeLang: (lang: Lang) => setLang(lang),
        lang
    };

    return (

        <AppContext.Provider value={contextValue}>
            <div>

                <select onChange={(e) => changeCountry(e.target.value)}>
                    <option value={'RU'}>Russia</option>
                    <option value={'UA'}>Ukraine</option>
                </select>

                <select onChange={(e) => changeLang(e.target.value)}>
                    <option value={'RU'}>RU</option>
                    <option value={'UA'}>UA</option>
                </select>

                <div className='holidays_wrapper'>
                    <div className='non-sorted_holidays'>
                        <p>Non sorted</p>
                        {holidays.map((holiday:any, index:number) =>
                            <ul key={index}>
                                <li>
                                    <p>
                                        {holiday.localName} - {holiday.date}
                                    </p>
                                </li>
                            </ul>
                        )}
                    </div>
                    <div className='sorted_holidays'>
                        <p>Sorted</p>
                        {sortedHolidays.map((holiday:any, index:number) =>
                            <ul key={index}>
                                <li>
                                    <p>
                                        {holiday.localName} - {holiday.date}
                                    </p>
                                </li>
                            </ul>
                        )}
                    </div>
                </div>
            </div>
        </AppContext.Provider>
    )
}
