export type Country = 'UA' | 'RU' ;


export type Holidays = [{
    "date": string,
    "localName": string,
    "name": string,
    "countryCode": string,
    "launchYear":number,
}];

export type Lang = 'UA' | 'RU' ;